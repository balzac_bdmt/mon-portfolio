//close menu
function closeMenu() {
  var closeBtn = document.getElementById("close");
  var header = document.getElementById("header");
  var main = document.getElementById("main");
  var footer = document.getElementById("footer")
  if (header.style.display === "none") {
    closeBtn.classList.remove('fa-bars');
    closeBtn.classList.add('fa-arrow-left');
    header.style.display = "";
    main.style.marginLeft = "300px";
    footer.style.marginLeft = "300px";
  } 
  else {
    closeBtn.classList.add('fa-bars');
    closeBtn.classList.remove('fa-arrow-left');
    header.style.display = "none";
    main.style.marginLeft = "0";
    footer.style.marginLeft = "0";
  }
}


// Get the modal
var modalGeoWorld = document.getElementById('modalGeoWorld');
var modalPokemonFront = document.getElementById('modalPokemonFront');
//var modalMust = document.getElementById('modalMust');
var modalEasyStage = document.getElementById('modalEasyStage');
var modalAFDL = document.getElementById('modalAFDL');
var modalDefipix = document.getElementById('modalDefipix');
var modalMorpion = document.getElementById('modalMorpion');

// Get the button that opens the modal
var btnGeoWorld = document.getElementById("BtnGeoWorld");
var btnPokemon = document.getElementById("BtnPokemonFront");
//var btnMust = document.getElementById("BtnMust");
var btnEasyStage = document.getElementById("BtnEasyStage");
var btnAFDL = document.getElementById("BtnAFDL");
var btnDefipix = document.getElementById("BtnDefipix");
var btnMorpion = document.getElementById("BtnMorpion");

// When the user clicks the button, open the modal 
btnPokemon.onclick = function() {
  modalPokemonFront.style.display = "block";
}
btnGeoWorld.onclick = function() {
  modalGeoWorld.style.display = "block";
}
// btnMust.onclick = function() {
//   modalMust.style.display = "block";
// }
btnEasyStage.onclick = function() {
  modalEasyStage.style.display = "block";
}
btnAFDL.onclick = function() {
  modalAFDL.style.display = "block";
}
btnDefipix.onclick = function() {
  modalDefipix.style.display = "block";
}
btnMorpion.onclick = function() {
  modalMorpion.style.display = "block";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
  if (event.target == modalPokemonFront) {
    modalPokemonFront.style.display = "none";
  }
  if (event.target == modalGeoWorld) {
    modalGeoWorld.style.display = "none";
  }
  // if (event.target == modalMust) {
  //   modalMust.style.display = "none";
  // }
  if (event.target == modalEasyStage) {
    modalEasyStage.style.display = "none";
  }
  if (event.target == modalAFDL) {
    modalAFDL.style.display = "none";
  }
  if (event.target == modalDefipix) {
    modalDefipix.style.display = "none";
  }
  if (event.target == modalMorpion) {
    modalMorpion.style.display = "none";
  }
}

//Chevron
//get chevron
var javaChevron = document.getElementById('javaChevron');
var phpChevron = document.getElementById('phpChevron');
var jsChevron = document.getElementById('jsChevron');

//open
function toggle_visibility_java() {
  var x = document.getElementById("androidStudioTR");
  if (x.style.display === "none") {
    x.style.display = "";
    javaChevron.className = "chevron-2";
  } 
  else {
    x.style.display = "none";
    javaChevron.className = "chevron";
  }
}

function toggle_visibility_php() {
  var x = document.getElementById("symfonyTR");
  if (x.style.display === "none") {
    x.style.display = "";
    phpChevron.className = "chevron-2";
  } 
  else {
    x.style.display = "none";
    phpChevron.className = "chevron";
  }
}

function toggle_visibility_js() {
  var jquery = document.getElementById("jqueryTR");
  var vue = document.getElementById("VueTR");
  if (jquery.style.display === "none") {
    vue.style.display = "";
    jquery.style.display = "";
    jsChevron.className = "chevron-2";
  } 
  else {
    vue.style.display = "none";
    jquery.style.display = "none";
    jsChevron.className = "chevron";
  }
}

//open image

var modal = document.getElementById('ImageBigger');
var modalImg = document.getElementById("imgBigScreen");
var captionText = document.getElementById("caption");
function openImg(img) {
  if (img.className === 'portrait') {
    document.getElementById("imgBigScreen").style.width = "30%";
  }
  else {
    document.getElementById("imgBigScreen").style.width = "90%";
  }
  modal.style.display = "block";
  modalImg.src = img.src;
  captionText.innerHTML = img.alt;
}

var span = document.getElementsByClassName("close")[0];
span.onclick = function() { 
  modal.style.display = "none";
}